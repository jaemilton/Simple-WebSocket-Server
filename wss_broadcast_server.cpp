#include "server_wss.hpp"

using namespace std;

using WssServer = SimpleWeb::SocketServer<SimpleWeb::WSS>;

int main(int argc, char *argv[]) {


  // WebSocket Secure (WSS)-server at port 8080 using 1 thread
  std::string crt_file = "mqtt-server.crt";
  std::string key_file = "mqtt-server.key";
  std::string ca_file = "mqtt-ca.crt";

  cout << "Received " << argc << " args" << endl;
  for (int i = 0; i < argc; i++)
  {
    cout << "argv: " << i << " value: " << argv[i] << endl;
  }

  //may return 0 when not able to detect
  const auto processor_count = std::thread::hardware_concurrency();
  unsigned int thread_number_per_cpu = 1;

  if (argc >= 4) {
    crt_file = argv[1];
    key_file = argv[2];
    ca_file = argv[3];
    if (argc == 5) {
      std::string str(argv[4]);
      thread_number_per_cpu = std::stoul(str,nullptr,0);
    }
  }

  cout << "Server certificate path " << crt_file << endl;
  cout << "Server certificate key path " << key_file << endl;
  
  WssServer server(crt_file, key_file, ca_file);
  //server.config.port = 8080;
  server.config.thread_pool_size = processor_count * thread_number_per_cpu;
  server.config.fast_open = true;
  

  cout << "Server thread_pool_size " << server.config.thread_pool_size << endl;

  //Regular expression to any URL path from 
  //https://www.oreilly.com/library/view/regular-expressions-cookbook/9780596802837/ch07s12.html
  //it will matchs only for lower case URLs
  auto &echo_all = server.endpoint["([a-z][a-z0-9+\-.]*:(//[^/?#]+)?)?([a-z0-9\-._~%!$&'()*+,;=:@/]*)"];

  echo_all.on_message = [&server](shared_ptr<WssServer::Connection> connection, shared_ptr<WssServer::InMessage> in_message) {

    // echo_all.get_connections() can also be used to solely receive connections on this endpoint
    //Send the messafe for all connections with the same path that is not it own
    for(auto &a_connection : server.get_connections()){
      //if is the same connection or different connection path, check next
      if (a_connection.get() == connection.get() || a_connection->path != connection->path) {
        continue;
      }

      a_connection->send(in_message->string(), nullptr, 130U);
    }
  };

  echo_all.on_open = [&server](shared_ptr<WssServer::Connection> connection) {
    cout << "Server: Opened connection " << connection.get() << " listening to path " << connection->path << endl;
  };

  // See RFC 6455 7.4.1. for status codes
  echo_all.on_close = [](shared_ptr<WssServer::Connection> connection, int status, const string & /*reason*/) {
    cout << "Server: Closed connection " << connection.get() << " with status code " << status << endl;
  };

  // Can modify handshake response headers here if needed
  echo_all.on_handshake = [](shared_ptr<WssServer::Connection> /*connection*/, SimpleWeb::CaseInsensitiveMultimap & /*response_header*/) {
    return SimpleWeb::StatusCode::information_switching_protocols; // Upgrade to websocket
  };

  // See http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html, Error Codes for error code meanings
  echo_all.on_error = [](shared_ptr<WssServer::Connection> connection, const SimpleWeb::error_code &ec) {
    cout << "Server: Error in connection " << connection.get() << ". "
         << "Error: " << ec << ", error message: " << ec.message() << endl;
  };

  // Start server and receive assigned port when server is listening for requests
  promise<unsigned short> server_port;
  thread server_thread([&server, &server_port]() {
    // Start server
    server.start([&server_port](unsigned short port) {
      server_port.set_value(port);
    });
  });
  cout << "Server listening on port " << server_port.get_future().get() << endl
       << endl;

  server_thread.join();
  
}


